import dropdownInit from "./dropdown";

const apiKey = "6b901f43b8a20a253289b70816fd70c5";
const cityName = document.querySelector('.city-name');
const dropdownMenu = document.querySelector('.dropdown-menu');
const cityElement = document.querySelector('.London');
const firstCity = cityElement.childNodes[0].nodeValue;
let Month, Day, Hours, Minutes;
let time = new Date();
let selectedCity;

window.addEventListener('DOMContentLoaded', () => {

  dropdownInit();
  
  dropdownMenu.addEventListener('click', (event) => {
    selectedCity = event.target.textContent;
    
    // We get the month, day, hours and minutes in different cities using the method toLocaleString()
    Month = time.toLocaleString('en-US', {month: 'long', timeZone: `Europe/${selectedCity}`});
    Day = time.toLocaleString('en-US', {day: 'numeric', timeZone: `Europe/${selectedCity}`});
    Hours = time.toLocaleString('en-US', {hour: 'numeric', hour12: false, timeZone: `Europe/${selectedCity}`});
    Minutes = time.toLocaleString('en-US', {minute: 'numeric', timeZone: `Europe/${selectedCity}`});
  });

  function getWeatherData(city) {
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}`)
      .then(response => response.json())
      .then(data => {
        const temperature = Math.round(data.main.temp -273.1);
        const wind = data.wind.speed.toFixed(1);
        const weatherIcon = data.weather[0].icon;
        const pressure = data.main.pressure;
        const city = data.name;
        const country = data.sys.country;
        const humidity = data.main.humidity;
        const visibility = data.visibility / 1000;
        const dewPoint = Math.round(temperature - ((100 - humidity) / 5));
        const description = data.weather[0].description;
        const feelsLike = Math.round(data.main.feels_like -273.1);
        const main = data.weather[0].main;


        const degrees = document.querySelector('.degrees');
        const degreesCard = document.querySelector('.degrees-card');
        const windCard = document.querySelector('.wind');
        const weatherIconElement = document.querySelector('.weather-icon');
        const cardWeatherIconElement = document.querySelector('.cardWeather-icon');
        const pressureElement = document.querySelector('.pressure');
        const cityElement = document.querySelector('.city');
        const humidityElement = document.querySelector('.humidity');
        const visibilityElement = document.querySelector('.visibility');
        const dewPointElement = document.querySelector('.dew-point');
        const weatherTextElement = document.querySelector('.weather-text');
        const weatherOur = document.querySelector('.weather-our');
        const dataTime = document.querySelector('.data-time');

        degrees.textContent = `${temperature} °C`;
        degreesCard.textContent = `${temperature} °C`;
        windCard.textContent = `wind: ${wind} m/s`;
        weatherIconElement.src = `https://openweathermap.org/img/w/${weatherIcon}.png`;
        cardWeatherIconElement.src = `https://openweathermap.org/img/w/${weatherIcon}.png`;
        pressureElement.textContent = `${pressure} hPa`;
        cityElement.textContent = `${city}, ${country}`;
        humidityElement.textContent = `Humidity: ${humidity}%`;
        visibilityElement.textContent = `Visibility: ${visibility}km`;
        dewPointElement.textContent = `Dew point: ${dewPoint}°C`;
        weatherTextElement.textContent = description;
        weatherOur.textContent = `Feels like ${feelsLike} °C. ${main}, ${description}`;
        dataTime.textContent = `${Hours}:${Minutes} pm, ${Month} ${Day}`;
      });
  }

  dropdownMenu.addEventListener('click', (event) => {
    selectedCity = event.target.textContent;
    getWeatherData(selectedCity);
  });

  const permission = confirm("Allow the use of location?");

  function getCityName(latitude, longitude) {
    const url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${latitude}&lon=${longitude}`;
    return fetch(url)
      .then(response => response.json())
      .then(data => {
        return data.address.city;
      });
  }

  function success(position) {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    getCityName(latitude, longitude)
      .then(city => {
        getWeatherData(city);
        localStorage.setItem('selectedCity', city);
      })
      .catch(error => {
        console.error(error);
      });
  }

  if (permission) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(success);
      let selectedCity = getLastSelectedCity() || 'firstCity';
      getWeatherData(selectedCity); // assuming getWeatherData is defined elsewhere in the code
      cityName.textContent = selectedCity;
      Month = time.toLocaleString('en-US', {month: 'long'});
      Day = time.toLocaleString('en-US', {day: 'numeric'});
      Hours = time.toLocaleString('en-US', {hour: 'numeric', hour12: false});
      Minutes = time.toLocaleString('en-US', {minute: 'numeric'});
    } else {
      alert("Your browser does not support geolocation.");
    }
  } else {
    let selectedCity = `${firstCity}`;
    getWeatherData(selectedCity);
    cityName.textContent = selectedCity;
    Month = time.toLocaleString('en-US', {month: 'long', timeZone: `Europe/${selectedCity}`});
    Day = time.toLocaleString('en-US', {day: 'numeric', timeZone: `Europe/${selectedCity}`});
    Hours = time.toLocaleString('en-US', {hour: 'numeric', hour12: false, timeZone: `Europe/${selectedCity}`});
    Minutes = time.toLocaleString('en-US', {minute: 'numeric', timeZone: `Europe/${selectedCity}`});
  }

  function getLastSelectedCity() {
    const city = localStorage.getItem('selectedCity');
    return city ? city : null;
  }
});

