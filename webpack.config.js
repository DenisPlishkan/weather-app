
const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const $ = require('jquery');

module.exports = {
    mode: "development",
    entry: ['./front/main.js', './front/main.css'],
    output: {
        path: path.resolve(__dirname, "./dist/front"),
        filename: "main.js"
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: path.resolve(__dirname, "./front/index.html"),
            filename: "index.html",
            minify: true
        }),
        new CopyWebpackPlugin({
            patterns: [
                { from: './front/main.css', to: 'main.css' },
            ],
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        })
    ],
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            }
        ],
    },
    devServer: {
        static: path.join(__dirname, 'dist'),
        compress: true,
        port: 3001,
        liveReload: true,
        hot: false 
    },
};